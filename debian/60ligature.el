;; Enable the www ligature in every possible major mode
(ligature-set-ligatures 't '("www"))

;; Enable ligatures in programming modes
(ligature-set-ligatures
 'prog-mode
 '(

   ;; Fira Code ligatures according to from
   ;; https://github.com/tonsky/FiraCode/wiki/Emacs-instructions
   "www" "**" "***" "**/" "*>" "*/" "\\\\" "\\\\\\" "{-" "::"
   ":::" ":=" "!!" "!=" "!==" "-}" "----" "-->" "->" "->>"
   "-<" "-<<" "-~" "#{" "#[" "##" "###" "####" "#(" "#?" "#_"
   "#_(" ".-" ".=" ".." "..<" "..." "?=" "??" ";;" "/*" "/**"
   "/=" "/==" "/>" "//" "///" "&&" "||" "||=" "|=" "|>" "^=" "$>"
   "++" "+++" "+>" "=:=" "==" "===" "==>" "=>" "=>>" "<="
   "=<<" "=/=" ">-" ">=" ">=>" ">>" ">>-" ">>=" ">>>" "<*"
   "<*>" "<|" "<|>" "<$" "<$>" "<!--" "<-" "<--" "<->" "<+"
   "<+>" "<=" "<==" "<=>" "<=<" "<>" "<<" "<<-" "<<=" "<<<"
   "<~" "<~~" "</" "</>" "~@" "~-" "~>" "~~" "~~>" "%%"

   ;; Ligatures not in Fira Code but in Jetbrains Mono, taken from
   ;; https://www.jetbrains.com/lp/mono/#font-family
   "--" "---" "=!=" "&&&" "&=" "?:" "?." "<:" ":<" ":>" ">:" "-|"
   "_|_" "|-" "||-" "#:" "#!" "#=" "<#--" "<==>" ">--" ">->" "<-<"
   "<-|" "<=|" "|=>" "|->" "<~>" "[||]" "|]" "[|" "|}" "{|" "[<"
   ">]" "||>" "<||" "|||>" "<|||" ".?" "::=" ":?" ":?>" "//=" "@_"
   "__"

   ))

;; Generally enable ligatures
(global-ligature-mode 't)
